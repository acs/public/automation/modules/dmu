# Description

This is a small tool that can manage structures inside python for multi threaded access and event based action when new data is written to an element.


# Send Control Command

curl -i -X POST 192.168.1.111:8087/set/sysControl/state/requestState "Content-Type: application/json" --data-binary '{"data" : "STANDBY"}'


## addRx
This is a alias for `addElmEvent`

## addElmMonitor
This is a alias for `addElmEvent`

## addElmEvent
The handler function passed to addRx can either be 3 or 4 or 5 parameters. The 3 and 4 parameter version is deprecated!!
`handlerXX(data, uuid, name, args, context)`
`handlerXX(data, uuid, name, args)`
`handlerXX(data, name, args)`

# Examples

## exampleMQTT_HTTP
This example provides rest as well as mqtt to interface the dmu with external services

Start the MQTT docker container by running

`cd docker`
`sudo docker-compose up`

Start the subscriber:

`mosquitto_sub -h localhost -p 1883 -t /test-topic`

Send commands to REST server:

`curl -i -X POST localhost:8087/set/dictTest/level1/level2 "Content-Type: application/json" --data-binary '{"data" : [2, 4, 9]}'`


## exampleSocket

This example provides mqtt to socket interface the dmu with external services

Start the MQTT docker container by running

`cd docker sudo docker-compose up`

Send command

`mosquitto_pub -h localhost -m "[1, 2, 3.4, -9]" -t /test-topic`

# FAQ

## Cannot push to git-ce:
The main repository can be found at the following URL. Please only push to the repository.

https://git.rwth-aachen.de/acs/public/automation/modules/dmu/

# Windows

I might be needed to change the execution policy for your system. Run the following in PowerShell as Admin

`Set-ExecutionPolicy -ExecutionPolicy Unrestricted`

For matlab you need to intall `Instrument Control Toolbox`

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.