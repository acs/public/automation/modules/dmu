from enum import Enum

class dataType(Enum):
    FLOAT = "f"
    INTEGER = "i"
    DOUBLE = "d"