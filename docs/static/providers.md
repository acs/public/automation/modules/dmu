# Providers

Provider extend the functionality of the dmu object storage. They hook onto the event system of specified objects and keep them synchronized with other parties over various means of communication and formats.

This is provided using two methods:

- `attachPublisher(...)`: From **dmu object to outside**
- `attachSubscriber(...)`: From **outside to dmu object**´
- `detachPublisher(...)`
- `detachSubscriber(...)`

> In the standard provider methods there is no option for bidirectional communication. The providers must internally dismiss the resulting feedback loop. It is better to use different dmu elements for server-client and client-server communication. 

Currently there are these first-party providers.

- [Socket](providers/socket.md)
- [MQTT](providers/mqtt.md)
- [Fiware](providers/fiware.md)
- [HTTP (rewrite)](providers/http.md)