# Fiware Provider API References

## \_\_init\_\_

```py
def __init__(self, dmuObj,  conf)
conf = {
    "fiwareHost" : str,             # Hostname of fiware instance
    "fiwarePort" : int,             # Port of fiware instance
    "subscriptionHost": str,        # Ip for receiving messages from fiware
    "subscriptionListener" : str,   # Address to bin the listener to
    "subscriptionPort" : int,       # Port for receiving messages from fiware
    "protocol" : str                # Protocol (http) 
}
```

Creates a new fiware client.

---

**Arguments**

- `dmuObj: dmu`: The dmu object the fiware provider operates on
- `conf: {}`: The configuration to use.

---

**Returns**

- `fiwareClient`

## cleanup

```py
def cleanup(self)
```

`cleanup` shutdown the fiware connection and removes all publisher and listeners.


## validateConf

```py
def validateConf(self, conf)
```

`validateConf` validates the given fiware client configuration.

---

**Arguments**

- `conf (config as above)`: The configuration to check as described in \_\_init\_\_

---

**Raises**

- `Exception`: "`Key missing in config. Please add {field}`"


## attachPublisher

```py
def attachPublisher(self, entityId, name, *args, dataField = "data")
```
`attachPublisher` enables a publisher via the local storage structure. Returns -1 in case of an error. Otherwise a uuid

---

**Arguments**

- `entityID (str)`: name of the public dmu element
- `name, *args (str, []str)`: path to the dmu element
- `dataField (str, option, default: "data")`: The field name of the data.

---

**Returns**

`-1` in case of an error. `UUID` of the publish subscription otherwise


## detachPublisher

```py
def detachPublisher(self, name, uuid)
```

`detachPublisher` removes the publishing subscription to an entity

---

**Arguments**

- `name (str)`: name of the dmu element
- `uuid (uuid.UUID)`: uuid of the subscription

---

**Returns**

`True` if the removal was successful, `False` otherwise


## attachSubscriber

```py
def attachSubscriber(self, entityId, name, *args, dataField = "data", federtationUrl = "")
```

`attachSubscriber` subscribes to entity. Returns -1 in case of an error. Otherwise a uuid.

---

**Arguments**

- `entityID (str)`: name of the public dmu element
- `name, *args (str, []str)`: path to the dmu element
- `dataField (str, option, default: "data")`: The field name of the data.
- `federtationUrl (str, optional, default: ""):` overwrite the subscription url if set

---

**Returns**

`-1` in case of an error. `UUID` of the subscription otherwise

## detachSubscriber

```py
def detachSubscriber(self, name, uuid)
```

`detachSubscriber` removes the subscription to an entity

---

**Arguments**

- `name (str)`: name of the dmu element
- `uuid (uuid.UUID)`: uuid of the subscription

---

**Returns**

`True` if the removal was successful, `False` otherwise


## createEntity

```py
def createEntity(self, entityId, model = simpleData)
simpleData = {
    "type" : "simpleData",
    "data" : {"value" : 0}
}
```
`createEntity` creates new entities

> currently only basic float is supported

---

**Arguments**

- `entityID (str)`: name of the entity to create
- `model (simpleData)`: the structure of the entity

---

**Returns**

`True` if the creation was successful. `False` otherwise (see logs why not)

## deleteEntity

```py
def deleteEntity(self, entityId, model = simpleData)
```

`deleteEntity` deletes the entity with the given name and model

---

**Arguments**

- `entityID (str)`: name of the entity to delete
- `model (simpleData)`: the structure of the entity

---

**Returns**

`True` if removal was successful, `False` otherwise


## connectFiware

```py
def connectFiware(self)
```

`connectFiware` is internally in `__init__` used to connect to attach to the fiware instance

## handleReceiver

```py
def handleReceiver(self, data, uuid, name, args, context)
```

`handleReceiver` is internally used to receive the data

## handlePublish

```py
def handlePublish(self, data, uuid, name, args, context)
```

`handlePublish` is internally used to receive the data