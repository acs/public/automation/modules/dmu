# HTTP Endpoint specifications

To get the path `dmu -> name -> subelem1` one would request. `GET: host:port/name/subelem1`

Methods:

- GET:
    - /\<path> -> returns the data of the element if it exists. If name is empty the whole dmu can be queried. If not the name is prefixed to the path

- POST:
    - /\<path> -> puts the data into the element if it does not exist

- PUT: 
    - /\<path> -> puts the data into the element if it exists but throws if path element does not exist

- DELETE:
    - /\<path> -> deletes the element if it exists

- OPTIONS: 
    - sends cors headers for Access-Control-Allow-Origin: *.