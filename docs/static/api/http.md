# HTTP Provider References

## \_\_init\_\_

```py
def __init__(self, dmuObj: dmu, config: HTTPSrvConfig)
```

`httpSrv` creates a new HTTP server provider.

---

**Arguments**

- `dmuObj: dmu`: The dmu object to operate on
- `config: HTTPSrvConfig`: configuration of functions(e.g.timeouts and timers)

---

**Returns**

The `httpSrv` instance.

---


## attachSubscriber

```py
def attachSubscriber(self, host: str, port: str, name:str) -> uuid.UUID
```
attachPublisher` opens a new server and lets other servers subscribe (request) the data. This is done using a separate thread. This function returns once the http server starts. 

---

**Arguments**
- host (str): hostname of the server (most likely localhost/127.0.0.1/0.0.0.0)
- port (int): the port the webserver will accept connections on
- name (str): Name of the dmu element that will be exposed. Leave empty to expose the whole dmu

---

**Raises**
- If another http server is already started it raises a     'ServerAlreadyPublishingException'
- If the socket (host:port) could not be bound.

---

**Returns**
`uuid.UUID`: UUID of the publisher. Can be used to detach it using `detachSubscriber`

---

## detachSubscriber

```py
def detachSubscriber(self, uuid: uuid.UUID)
```
`detachSubscriber` closes the current http server.
When no server is open this will result in a noop.

---

**Arguments**
- `uuid (uuid.UUID)`: uuid of the http subscriber provider

---

## attachPublisher

```py

def attachPublisher(self, host: str, port: int, name: str) -> uuid.UUID

```

`attachPublisher` create a new elm event on the name (or all elements on `""`) and publishes/pushes these changes to the http server at `host:port`.

---

**Arguments**

- `host (str)`: The hostname of the target server
- `port (int)`: The port of the target server
- `name (str)`: The element of to publish. Leave empty for all elements (complete synchronization)


---

**Returns**

`uuid.UUID` the uuid identifying the publisher

---

## detachPublisher

```py
def detachPublisher(self, name: str, uuid: uuid.UUID) -> bool
```

`detachPublisher` detaches the publishing element event handler from the dmu object.

---

**Arguments**:

- `name (str)`: The name of the dmu object the publisher listens on. `""` if it's a global publisher uuid (`uuid.UUID`): The uuid of the publisher


---

**Raises**
- `NotImplementedError`: currently global dmu objects are not yet implemented

---

**Returns:**
bool: `true` when the detach was successful

---
## attachPoller

```py
def attachPoller(self, host:str, port: int, name = "") -> uuid.UUID
```

`attachPoller` polls the object at `host:port`.

---

**Arguments**

-  `host (str)`: host that should be polled
- `port (int)`: port that should be polled
- `name (str, optional)`: name of the dmu element that should be polled (Subelements are not supported). Defaults to all.
- `intervalS (float)`: interval of the poller
---

**Returns**

`uuid.UUID` used to identify the poller.

---

## detachPoller

```py
def detachPoller(self, uuid: uuid.UUID, name = "") -> bool
```
Detaches a poller from the element

**Arguments**

- `uuid (uuid.UUID)`: The uuid of the poller
- `name (str, optional)`: The dmu element the poller was registered on.

---

**Return**
`bool`: True if successful

---

## cleanup

```py
def cleanup(self)
```

`cleanup` shuts the server down and cleans up all resources.
