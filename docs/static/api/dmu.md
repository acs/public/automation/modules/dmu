# DMU References

## **__init__**

```py
def __init__(self)
```

Constructs a new DMU instance.

## __del__

```py
def __del__(self)
```

Deconstructs the dmu objects

## cleanup

```py
def cleanup(self)
```

Removes all event handlers

## **addSubElm**

> While reworking the user api this will most likely change

```py
def addSubElm(self, data, name, *args)
```

`addSubElm` adds new [subelements](overview.md#theory) with

---
**Arguments**

- `data: {dictionary or list}`: The data of the element (most likely a dictionary, list)
- ``name, *args: str``: path to the parent element where the subelements will be added.

---
**Returns**

Returns `int`

- $\ge 0$: list-index of the new subelement
- $-1$: Successfully added the dictionary
- $-2$: Error

Returns int, either the list-index (**?**) if $\ge 0$

## **addElm**

```py
def addElm(self, name, data, notify = False)
```

`addElm` adds a new (root) element (different from a [subelemnt](overview.md#theory)). This will not have the path'ed event handlers like subelements do.

---
**Arguments**

- `name: str`: Name of the element
- `data: any`: Arbitrary data to be stored
- `notify: bool`: whether to notify event handlers or not.\
    Defaults to `False`

---
**Returns**

Returns `bool`
- `True`: if storing and notifications were successful
- `False`: otherwise

## rmElm

```py
def rmElm(self, name)
```

Removes a element from the root object

---
**Arguments**

- `name: str`: Name of the element to be removed  

## **setDataSubset**

```py
def setDataSubset(self,data,name,*args)
```

Writes data into the subelements of the given path

---
**Arguments**
- `data: any`: The data to write
- `name, *args: str`: The path to write `data` to

---
**Returns**
A `bool` whether the write was successful

## **getDataSubset**

```py
def getDataSubset(self,name,*args)
```

`getDataSubset` reads the data from the given path

---
**Arguments**
- `name, *args: str`: The path to read from

---
**Returns**
Returns the data `:any` if it exists `None` otherwise



## **addElmEvent**

```py
def addElmEvent(self, handle, name, *args, blockUntilStart = True, context = None)
```
Adds a event handler to a specific path. This handler will be invoked once the element is modified.

This event handler will be startet using a new thread. 

Once the handler was added, a *StartEvent* will be invoked

---
**Arguments**
- `handle: any`: The handler that should be added //TODO: handle type
- `name, *args: str`: The path to the subelement
- `blockUntilStart: bool`: Wether to block the event handler thread until the start event is invoked\
    Defaults to `True`
- `context: any`: The context that should be passed on to the event thread\
    Defaults to `None`

---
**Returns**
Returns a `str` uuid of the added handler


## rmElmEvent

```py
def rmElmEvent(self, name, uuid)
```

`rmElmEvent` removes a event handler from the dmu

---
**Arguments**
- `name: str`: Name of the handler
- `uuid: str`: UUID of the handler

---
**Returns**

A `bool` whether the removal was successful




## notifyElmEvent

```py
def notifyElmEvent(self, name, uuid)
```

This notifies a specific event

---
**Arguments**
- `name: str`: name of the element
- `uuid: {str, int}`: UUID of the event

## generateNotifiers


```py
def generateNotifiers(self, target, name, data = None)
```

`generateNotifiers` generates the internal Notifiers, that are the internal barriers used to _"block"_ an event handler thread until it is invoked.

---
**Arguments**

- `target: str`: 
- `name: str`: Name of the element
- `data: any`:

## cleanNotifiers

`cleanNotifiers` removes the internal notifiers (barriers) from the target

```py
def cleanNotifiers(self, target, name, subElmList = None)
```

---
**Arguments**
- `target: str`
- `name: str`
- `subElmList: list`

## buildNamesMap

```py
def buildNamesMap(self, data, listIn)
```

---
**Arguments**
- `data: {dict, list}`:
- `listIn: list`

---
**Returns**
Returns `list`

## getAllElementNames

```py
def getAllElementNames(self):
```

Get all element names. (Using [buildNamesMap](#buildnamesmap))

## elmEventThread

```py
def elmEventThread(self, name, uuid, startEvent, context)
```

`elmEventThread` is internally used for each event handler. This gives every handler its own thread.

---
**Arguments**


## getThreadUuid

```py
def getThreadUuid(self,name, *args)
```

Get's the thread id if it exists

---
**Arguments**
- `name, *args: {st}`: Path of the subelement

## recursiveEventHandle

```py
def recursiveEventHandle(self, data, uuid, name, args)
```

Triggers all event handlers for *all** subelements below the given path.

**rekursive event handlers can be attached with [`attachRecursiveEvent`](#attachrecursiveevent) 

---
**Arguments**
- `data: any`: Unused
- `uuid: str`: 
- `name, *args`: Path to the subelement

## attachRecursiveEvent

```py
def attachRecursiveEvent(self, name, *args)
```
Add rekursive event handlers, that invokes every subelement below the given path

---
**Arguments**

- `name, *args: str`: The path to the subelement

---
**Returns**

A uuid `:str` of the added handler   

## detachRecursiveEvent

```py
def detachRecursiveEvent(self, name, uuid)
```

`detachRecursive`Removes the given event handler from the subelement

---
**Arguments**

- `name: str`: name of the Handler
- `uuid: str`: uuid of the handler



## eventLastUpdate

```py
def eventLastUpdate(self, name, uuid)
```

Gets the last invoked event of a event handler (thread).

---
**Arguments**
- `name: str`: Name of the handler
- `uuid: str`: UUID of the handler

---
**Returns**
- The last invoked event of the handler
- $-1$ in case of an error

## eventTriggerCounter

```py
def eventTriggerCounter(self, name, uuid)
```

`eventTriggerCounter` get the number of the handler invocations. 

---
**Arguments**
- `name: str`: Name of the handler
- `uuid: str`: UUID of the handler

---
**Returns**
- The invocation count
- $-1$ in case of an error




## setDataSubsetList

```py
def setDataSubsetList(self,args)
```

`setDataSubsetList` treats `args[0]` as the data and `args[1:]` as the path to be written to. 

This wraps [setDataSubset](#setdatasubset)


## getNotifierHandle

```py
def getNotifierHandle(self, target, argsList)
```

`getNotifierHandle` notifier handles (barrier) of the target

---
**Arguments**
- `target: any`:
- `argList: list`


## getDataSubsetUpdate

```py
def getDataSubsetUpdate(self,name,*args)
```

`getDataSubsetUpdate` reads a subset from the given path. **This is the blocking version of [getDataSubset](#getdatasubset)**

---
**Arguments**
- `name, *args: str`: The path to read from

---
**Returns**

Returns a tuple `(bool, any)` of the status `:bool` and the data `:any` if it exists `None` otherwise

## elmExists

```py
def elmExists(self, name, *args)
```

Checks wether a element at the given path exists or not.

---
**Arguments**

- `name, *args: str`: The path to check

---
**Returns**
A `bool` of the result.  




## addRx

> :warning: **Deprecated in favor of [addElmEvent](#addelmevent)

```py
def addRx(self, handle, name, *args)
```
Adds a event handler to a given path

---
**Arguments**

- `handle: any`: The event handler to be added
- `name, *args: str`: The path where it should be added

---
**Returns**

A `str` uuid of the added handler

## addElmMonitor

> :warning: **Deprecated in favor of [addElmEvent](#addelmevent)

```py
def addElmMonitor(self, handle, name, *args)
```
Adds a event handler to a given path

---
**Arguments**

- `handle: any`: The event handler to be added
- `name, *args: str`: The path where it should be added

---
**Returns**

A `str` uuid of the added handler

## rmElmMonitor

> :warning: **Deprecated in favor of [rmElmEvent](#rmelmevent)

```py
def rmElmMonitor(self, name, uuid)
```

`rmElmMonitor1` removes a event handler from the dmu

---
**Arguments**
- `name: str`: Name of the handler
- `uuid: str`: UUID of the handler

---
**Returns**

A `bool` whether the removal was successful

## rmRx

> :warning: **Deprecated in favor of [rmElmEvent](#rmelmevent)

```py
def rmRx(self, name, uuid)
```

`rmRx` removes a event handler from the dmu

---
**Arguments**
- `name: str`: Name of the handler
- `uuid: str`: UUID of the handler

---
**Returns**

A `bool` whether the removal was successful