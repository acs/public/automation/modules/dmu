# MQTT Provider 

MQTT is a protocol for machine-to-machine communication. The dmu library contains first-party-support for publishing and subscribing to events via mqtt.
This is handled by the `mqttClient`. 

## MQTT Basic Example

```py
#dmu setup
dmuObj = dmu()
dmuObj.addElm("dictTest", {"level1": "test", "level1.2": {"level2": "test2"}})
dmuObj.addElm("listTest", [test])

#mqtt setup
mqttObj = mqttClient("mqtt", dmuObj)


#attach to level1
mqttObj.attachPublisher("/test-topic", "json", "dictTest", "level1")

```

## Publisher

```py
def attachPublisher(self, topic, formatIn, name,  *args)
```

attachPublisher subscribes to the data changes of the dmu object specified by the path and sends the changes out to the topic endpoint given the data format.

---
**Arguments**

- `topic: str`: The mqtt topic to send the data to. If **none** is provided, it implies that the topic is unspecified (answers to this can vary based on the mqtt broker).
- `formatIn: str`: A indicator of the data format should be used with the transmission. **Currently only json is supported** and it will fallback to json
- `name, *args: str`: The path to the dmu object that should be subscribed to and published.

---
**Return**
A `Boolean` indication whether the publishing registration succeeded.

## Subscriber
```py
def attachSubscriber(self, topic, formatIn, name, *args)
```

`attachSubscriber` subscribes to a mqtt topic and pushes data when received into the specified path.
The received data will be deserialized using the formatIn serializer (only **json** is supported currently)

---
**Arguments**

- `topic: str`: The mqtt topic to subscribe to
- `formatIn: str`: The format that is expected to receive (only `json` currently supported)
- `name, *args`: The path to the 

--- 
**Return**
`None`
