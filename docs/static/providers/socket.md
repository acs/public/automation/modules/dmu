# Socket Provider 

The socket provider brings a simple interface that enables you to synchronize data with other clients and sources over socket connections (either _tcp_ or _udp_).

This can include [VILLASNode](https://villas.fein-aachen.org/doc/node.html) or others.

## Basic Example Client

```py
dmuObj = dmu()

socketObj = socketClient("127.0.0.1", 15001, dmuObj)

dmuObj.addElm("list", [])
dmuObj.addElm("dict", {"value1": "test"})

socketObj.attachPublisher("dict", "value1")

dmuObj.cleanup()
```

This adds two elements to the dmu so it looks like `dmu -> {list: [], dict: {value1: "test"}}`. Then it attaches a Publisher (see below) to `value1`.

This does the following:

## Publishers (socketClient)

```py
def attachPublisher(self, name, *args)
```

Publishers function as simple Event Handlers to the given path (`name+args`). Every time a change is made to this element (**!not recursive!**) and therefore the event handler triggered a packet with the change is transmitted through the socket.

---

**Arguments**
- `name, *args`: Path to the data input. This and future changes will be sent over the socket to the listeners.

---

## Basic Example Server

```py
dmuObj = dmu()

socketObj = socketServer("127.0.0.1", 15001, dmuObj, int)

dmuObj.addElm("dict", {})

socketObj.attachPublisher("dict")

## wait

dmuObj.cleanup()
```

This is the opposite from the example above we start a server and expect values of integers. Then we setup a element where we want our data to be stored in and call a:

## Subscriber (socketServer)

```py
def attachSubscriber(self, name: str, *args)
```
The other side of the synchronization is the subscriber from the socketServer. Here a socket is hosted when using the constructor.

Then a subscriber can be attached to a element path (`name+args`) so that whenever a connection sends a valid data packet a `setDataSubset(data, name, subpath)` is executed. 

---

**Arguments**
- `name, *args: str`: Path to the data output. This will get populated by the data coming over the socket

---

**Return**

- `str`: A uuid of the subscriber. This can be used to remove the subscriber when necessary

### Detaching

```py
def detachSubscriber(self, name: str, uuid: str) -> bool
```

detachSubscriber stops the socket subscriber at the given path with the given uuid.

---

**Arguments**

- `name: str`: The root-key where the subscriber is ankererd.
- `uuid: str`: The uuid of the subscriber instance.
---

**Return**

`bool` whether the detaching process was successful or not.


## Data Format

Because sockets only lay the foundation for communication, the dmu data can be transmitted using multiple formats.

> Currently only the [Custom Villas Binary](https://villas.fein-aachen.org/docs/node/formats/villas_binary) Version 1 format/protocol is supported

- [Custom Villas Binary Format](https://villas.fein-aachen.org/docs/node/formats/villas_binary) enables a fast binary transmission to villas node.

> There will be a selection of different formats available once implemented