# HTTP-Provider

The http provider is similar to the [socket](/providers/socket) though it uses the `http` protocol instead of the villas binary protocol.

> ⚠️ The HTTP Server uses the provider syntax (attachSubscriber/Publisher). This is useful within the dmu ecosystem, but from the network standpoint, this is unintuitive. The subscriber opens the http-server, the publisher is the http-client. 

## HTTPSrv

```py
def __init__(self, dmuObj: dmu, config: HTTPSrvConfig)
```

`httpSrv` creates a new HTTP server provider.

---

**Arguments**

- `dmuObj: dmu`: The dmu object to operate on
- `config: HTTPSrvConfig`: configuration of functions(e.g.timeouts and timers)

---

**Returns**

The `httpSrv` instance.

---
---

### Subscription

```py
def attachSubscriber(self, host: str, port: str, name:str) -> uuid.UUID
```
attachPublisher` opens a new server and lets other servers subscribe (request) the data. This is done using a separate thread. This function returns once the http server starts. 

> ⚠️ Keep in mind this opens a new http server and lets the other side **push** their changes to this dmu obj.

> Only one open publisher is allowed for one HttpSrv Publisher as of now. The uuid is for api compatibility and reserved for use later

---

**Arguments**
- host (str): hostname of the server (most likely localhost/127.0.0.1/0.0.0.0)
- port (int): the port the webserver will accept connections on
- name (str): Name of the dmu element that will be exposed. Leave empty to expose the whole dmu

---

**Raises**
- If another http server is already started it raises a     'ServerAlreadyPublishingException'
- If the socket (host:port) could not be bound.

---

**Returns**
`uuid.UUID`: UUID of the publisher. Can be used to detach it using `detachSubscriber`

---
---

```py
def detachSubscriber(self, uuid: uuid.UUID)
```
`detachSubscriber` closes the current http server.
When no server is open this will result in a noop.

---

**Arguments**
- `uuid (uuid.UUID)`: uuid of the http subscriber provider

---
---

### Publishing

```py

def attachPublisher(self, host: str, port: int, name: str) -> uuid.UUID

```

`attachPublisher` create a new elm event on the name (or all elements on `""`) and publishes/pushes these changes to the http server at `host:port`.

> Note listening to all elements is not implemented yet.

---

**Arguments**

- `host (str)`: The hostname of the target server
- `port (int)`: The port of the target server
- `name (str)`: The element of to publish. Leave empty for all elements (complete synchronization)


---

**Returns**

`uuid.UUID` the uuid identifying the publisher

---

```py
def detachPublisher(self, name: str, uuid: uuid.UUID) -> bool
```

`detachPublisher` detaches the publishing element event handler from the dmu object.

---

**Arguments**:

- `name (str)`: The name of the dmu object the publisher listens on. `""` if it's a global publisher uuid (`uuid.UUID`): The uuid of the publisher

---

**Raises**
- `NotImplementedError`: currently global dmu objects are not yet implemented

---

**Returns:**
bool: `true` when the detach was successful

---
---

```py
def cleanup(self)
```

`cleanup` shuts the server down and cleans up all resources.

> This will currently deadlock when the server is not started.

### Poller

After starting a http server (`attachSubscriber`) one can use the poller to poll in `pollerIntervalS` interval's the endpoint for the entire state of the element. For more information see the references 

## HTTP Endpoint Specification

See the API References of the HTTP Endpoint.


