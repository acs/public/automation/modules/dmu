# Fiware Provider

Works similarly to the other providers.

First we must initialize the Fiware client

```py
client = fiwareClient(dmuObj, {
    "fiwareHost" : "localhost",                     # Hostname of fiware instance
    "fiwarePort" : 1026,                            # Port of fiware instance
    "subscriptionHost" : "host.docker.internal",    # Ip for receiving messages from fiware
    "subscriptionListener" : "0.0.0.0",             # Address to bin the listener to
    "subscriptionPort" : 8080,                      # Port for receiving messages from fiware
    "protocol" : "http"                             # Communication protocol  
    })
```

And create a new entity id

```py
entityID = "entity0"
client.createEntity(entityID)
```

Then we can use the standard provider methods.

```py
client.attachPublisher(entityID, "testSend") # publishes to the outside
client.attachSubscriber(entityID, "testReceive") # receives from the outside
```

These can later be detached.

```py
dmuObj.addElm("testSend", {})
client.detachPublisher(entityID, "testSend")

dmuObj.addElm("testReceive", {})
client.detachSubscriber(entityID, "testReceive")
```

## API 

For more details see [API References](/api/fiware).

