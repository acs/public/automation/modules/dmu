# HTTP-Provider

The http provider is similar to the [socket](/providers/socket) though it uses the `http` protocol instead of the villas binary protocol.

> This will change. The HTTP-Provider is not functional right now.

> Currently the HTTP-Provider functions quite a bit differently than the other providers. There is no Publisher/Subscriber.

# httpSrv

```py
def __init__(self, host, port, dmuObj)
```

`httpSrv` creates a new HTTP server with the given Port and dmuObj. The server can then be started using the `start` command (most probably in a new thread [see example])

---

**Arguments**

- `host: string`: The hostname of the server (most likely `"0.0.0.0"` or `"localhost"`)
- `port: int`: The port of the server.
- `dmuObj: dmu`: The dmu object to synchronize

---

**Returns**

The `httpSrv` instance.

---

```py
def start(self)
```

`start` Starts the server which then handles all requests. This will use `serve_forever()` so the thread is subsequentyl blocked by this server. 

> You should call this function in a new dedicated thread.

---

```py
def cleanup(self)
```

`cleanup` shuts the server down and cleans up all resources.

> This will currently deadlock when the server is not started.

## Connecting with the server

Suppose a server is running on Port `8080` on `dmuObj1`. We have another dmu store `dmuObj2` which we want to synchronize with the server.



