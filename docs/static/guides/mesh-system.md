# Mesh-System

The `examples/docker-mesh` example gives a simple introduction how a mesh system could be created using the dmu library. 

It uses the built-in `http`-provider. Both `controller` and `client` will have open http servers to communicate.

First of we must create a controller, that each client then uses to get information and signals from. The controller must save information for each client and save the number of clients connected to it.

```py
dmuObj = dmu()
dmuObj.addElm("control", {"clientCount": 0})
dmuObj.addElm("clients")
```

Then we can use the `dmu` event-system to register new clients

```py
dmuObj.addElm("registration", {})

def registerClient(data, uuid, name, args):
    #we check if client has already registered
    if not dmuObj.elmExists("clients", data):
        
        dmuObj.addSubElm({"init": "false"}, "clients", data)

        #connect to client server data:port
        dmuObj.addElmEvent(data, "clients", data)

        dmuObj.setDataSubset({"init": "true"}, "clients", data)
        #add to clientCount
        dmuObj.setDataSubset((dmuObj.getDataSubset("control", "clientCount") + 1), "control", "clientCount")

dmuObj.addElmEvent(registerClient, "registration")
```

Now when a client uses the http server to add it's `ip.port` to the `registration` element the new client is registered and a connection will be established. 

The client wait until the `{init: true}` subset is set. From then a update to the `clients:hostname:port` can a command or a disconnect message.

The client's code is similar to the servers:

```py
# start http server

dmuObj = dmu()
dmuObj.addElm("clients", {})
dmuObj.addSubElm({}, "clients", myHostname)

def controllerUpdate(data, uuid, name, args):
    if data = "disconnect":
        handle_controller_disconnect()
    logging.info("Controller Update" + str(data))

dmuObj.addElmEvent(controllerUpdate, "clients", myHostname)

# register client to controller
dmuObj.addElm("registration", {})

# syncronize registration object

dmuObj.setDataSubset(hostname + ":" + port, "registration")

```

Now the clients calls the controller to register which establishes a channel. Which is then used to update the client from
 the server.
