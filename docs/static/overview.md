# Overview / Get-Started   

This will overview will give you an introduction to the dmu library.

> Due to active development, there may be API change. Attention is paid to backwards compatibility

## Basic Example

```py
    # define event handlers
    def listChanged(data, uuid, name, args, context):
        logging.info("list changed")

    def dictChanged(data, uuid, name, args, context):
        logging.info("dict changed")

    def subelementChanged(data, uuid, name, args, context):
        logging.info("subelement changed")


    dmuObj = dmu()

    #create elements
    dmuObj.addElm("listTest",[])
    dmuObj.addElm("mixedTest",{"level1" : { "list" : [] }})

    #register handlers on different elements and subelements
    dmuObj.addElmEvent(listChanged,"listTest")
    dmuObj.addElmEvent(subelementChanged,"dictTest","level1","level2")

    dmuObj.setDataSubset([1,2,3],"listTest") #overwrite a list
```

More examples are found in the detailed documentation or the `examples` directory in the repo.

## Theory

The DMU library is in it's simplest form a distributed thread safe Key-Value structure, that can be synchronized with external instances with event/change handling built in.
One key advantage are the subelements, that divide the structure into segments.

The whole deal of this library is to remove the burdens of multithreaded and or remote memory access. You should just use dmu objects like lokal vars.

## Base Unit

The core of this library consists of the [dmu](api/dmu.md) object. This has fields, that can be addressed by name. You can also add [subelements](api/dmu.md#addsubelm), that have the benefit of standalone event handlers. Subelements usually are typeof `dict` or `list` for this reason.

### Terminology

[//]: # (This is cluttered but it is what it is)

| Name       | Explanation                                                                                                            |  Functionality   |
| ---------- | ---------------------------------------------------------------------------------------------------------------------- | --- |
| DMU Object | The dmu object is the core of this library see it as the dictionary object in other libraries                          | Core    |
| Subelement | We can divide the dmu object into multiple subelements that each hold values. These trigger events only in this subset | Core    |
| Events | Events are invoked each time a part of the dmu object (or subelements) changed. These can be set as recursive| Core |
| Event Handler | Event handlers can be registered to a part of the dmu that are invoked whenever a event is transmitted | Core |

## Basics

The start ist always the root dmu object (or multiple; from now on called  the **root** object).

```py
dmuObj = dmu()
```

To use the dmu just add Elements.
Elements, that can either be values, or containers like dictionaries or lists. 

Each Item has a name (or key) and a value. 

```py
dmuObj.addElm("testkey", "testval")

dmuObj.addElm("testDictionary", {"testkey": "anotherVal"})
```

But the dmu not only works at the root (top) level it supports containers. 
To specify an exact location in the dmu a path name can be used. 

In most functions a `name, *args` parameter must be given. This specifies the path. The `name` is the elm from the root dmu object. The `*args` is the used for subelement if any.

So to give the path of `anotherVal` would be `name: "testDictionary"; *args: ["testkey"]`. Because of the python syntax `name: "testDictionary"; *args: "testkey"` would also be correct.


```mermaid
sequenceDiagram
    participant Application A
    participant DMU
    participant Event-Thread
    participant Application B

    activate Application A
    Application A->>DMU: dmu()
    activate DMU
    DMU-->>Application A: DMU Instance
    Application A ->> DMU: addElm("testkey", "testval")
    Application A ->> DMU: addElm(...)
    deactivate Application A
    deactivate DMU
```

When you want to change the value of that element just use the [setDataSubset](api/dmu.md#setDataSubset) method on the root object.

```py
dmuObj.setDataSubset("newValue", "testDictionary", "testkey")
```

This will then only update the val1, so that `val1 = newValue`, which can be retrieved with.

```py
val = dmuObj.getDataSubset("testDictionary", "testkey") # val would be newValue
```

```mermaid
sequenceDiagram
    participant Application A
    participant DMU
    participant Event-Thread
    participant Application B

    activate Application A
    Application A->>DMU: dmu()
    activate DMU
    DMU-->>Application A: DMU Instance
    Application A ->> DMU: addElm("testkey", "testval")
    Application A ->> DMU: addElm(...)

    Application A ->> DMU: setDataSubset(newValue, ...) 
    Application A ->> DMU: getDataSubset(...)
    DMU ->> Application A: "newValue"
    deactivate Application A
    deactivate DMU
```

The usefulness of the dmu object comes from the [addElmEvent](api/dmu.md#addElmEvent) method, which you can use to add handlers to a specific path (more on that later) of the dmu object. That handler will then be invoked each time the element **or subelement** change.

```py
# handler declaration
def dict2Changed(data, uuid, name, args, context):
    log(this)

dmuObj.addElmEvent(dict2Changed, "dict1", "dict2") # add handler to path dict1/dict2
```

Each time we overwrite (using [setDataSubset](api/dmu.md#setDataSubset)) either the dictionary or it's children, `dict2Changed` would be invoked with the context of that change.

These were the basics, ongoing from here are multiple steps to learn the advanced feature sets of this library.

- Examples
- Providers
  - [http server/client](http.md)
  - [mqtt server/client](mqtt.md)
  - [firewire server/client](firewire.md)
- [API References](api.md)

```mermaid
sequenceDiagram
    participant Application A
    participant DMU
    participant Event-Thread
    participant Application B

    activate Application A
    Application A->>DMU: new
    activate DMU
    DMU-->>Application A: DMU Instance

    Application A ->> DMU: addElm()

    deactivate Application A
    deactivate DMU
```

## About threads and parallel programming
> Following the restructuring and optimizing of this library this **might** change.

This library is being kept thread safe. But more...

Each event handler lives on its own thread. It uses a barrier, so we avoid polling and blocking cpu time.