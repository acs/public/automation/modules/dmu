from datetime import datetime
from time import sleep
from dmu.grafana import Grafana
from dmu.dmu import Dmu
import coloredlogs, logging

coloredlogs.install(
    level="DEBUG",
    fmt="%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s",
    field_styles=dict(
        asctime=dict(color="green"),
        hostname=dict(color="magenta"),
        levelname=dict(color="white", bold=True),
        programname=dict(color="cyan"),
        name=dict(color="blue"),
    ),
)

logging.info("Program Start")


data = {"active_power": [], "reactive_power": []}
dmuObj = Dmu()
dmuObj.add(data,"example")
endpoint = Grafana(dmuObj, "localhost", 8000)
endpoint.attachPublisher()

# data structure to be fill over time


# Data for example
P = [0.21852, 0.21138, 0.18086, 0.11853, 0.11505, 0.14528, 0.23082, 0.038349, 0.17539, 0.27034, 0.092701, 0.13183, 0.27859, 0.18238, 0.071194, 0.1642, 0.13435, 0.16508, 0.18337, 0.13246, 0.12642, 0.11164, 0.14679, 0.13298, 0.11949, 0.1707, 0.16472, 0.11953, 0.15899, 0.24153, 0.00085177, 0.17633, 0.15091, 0.06883, 0.12048, 0.20863, 0.092202, 0.06059, 0.14704, 0.13514, 0.11473, 0.12699, 0.14659, 0.13067, 0.13733, 0.20305, 0.04638, 0.074122, 0.22438, 0.10233, 0.14641, 0.056379, 0.12366, 0.20889, 0.090998, 0.1255, 0.17848, 0.058396, 0.064838]
Q = [0.06426, 0.064309, 0.06435, 0.064384, 0.064411, 0.06443, 0.064443, 0.064449, 0.064447, 0.064439, 0.064424, 0.064403, 0.064374, 0.06434, 0.064298, 0.064251, 0.064197, 0.064137, 0.064071, 0.063998, 0.06392, 0.063836, 0.063746, 0.063651, 0.06355, 0.063443, 0.063331, 0.063214, 0.063091, 0.062963, 0.062831, 0.062693, 0.062551, 0.062404, 0.062252, 0.062096, 0.061936, 0.061771, 0.061602, 0.061429, 0.061252, 0.061071, 0.060886, 0.060698, 0.060506, 0.060311, 0.060112, 0.059911, 0.059706, 0.059498, 0.059287, 0.059074, 0.058857, 0.058639, 0.058417, 0.058194, 0.057968, 0.05774, 0.05751]

try:
    while True:
        for p, q in zip(P, Q): 
            time = datetime.now().timestamp() * 1_000 # to unix_ts in ms
            # simulate data being measured one by one
            data["active_power"].append([p, time]) 
            data["reactive_power"].append([q, time])
            dmuObj.setData(data,"example")
            sleep(1) # wait to simulate measurement interval

except KeyboardInterrupt:
    logging.warning("Manual shut down")

endpoint.detachPublisher()


endpoint.cleanup()
