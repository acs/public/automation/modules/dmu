# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

# imports

from socket import socket
import sys
from pathlib import Path
import threading

def test(data, uuid, name, subelements): 
    print("Element update: " + name)
    print(data)

# Add project path to system path to facilitate imports of teebco submodules
project_dir = Path(__file__).resolve().parents[1]
sys.path.append(str(project_dir))

from dmu.dmu import dmu
from dmu.mqttClient import mqttClient
from dmu.socketClientSrv import socketClient
from dmu.socketServer import socketServer
import coloredlogs, logging
import time, sys

# Configure logging
coloredlogs.install(
level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")

# dmu initialization
dmuObj = dmu()

# Open socket connection
#socketSrv = socketServer("127.0.0.1", 15001, dmuObj)
#socketSrv.accept()

#time.sleep(5)

logging.debug("Connecting to socket server")

socketObj = socketClient("127.0.0.1", 15001, dmuObj)

#socketServer = socketServer("127.0.0.1", 15001, dmuObj)

# Create DMU Objects
dmuObj.addElm("listTest",[])
dmuObj.addElm("dictTest",{"level1" : { "level2" : "testval" }})
dmuObj.addElm("mixedTest",{"level1" : { "list" : [] }})



# Register Handlers
#socketObj.attachPublisher("dictTest", "level1", "level2")


time.sleep(1)

# Register Data Subset
dmuObj.setDataSubset(list(range(1,17)),"dictTest","level1","level2")


logging.info("start send")


# Send updated data
#for j in range(10):
#    time.sleep(1)
#    dmuObj.setDataSubset([1, 2],"dictTest","level1","level2")

dmuObj.addElmMonitor(test, "dictTest", "level1", "level2")


logging.info("end send")

