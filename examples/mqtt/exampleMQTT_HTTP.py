# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

##############################################################################
#
# This file shows how to use mqtt in combination with the REST api
#
#
##############################################################################

import sys
from pathlib import Path

# Add project path to system path to facilitate imports of teebco submodules
project_dir = Path(__file__).resolve().parents[1]
sys.path.append(str(project_dir))

from dmu.dmu import Dmu
from dmu.mqttClient import MqttClient
from dmu.httpSrv import httpSrv
import coloredlogs, logging
import time, sys

def listChanged(data, name, handle):
    logging.info("list changed")


coloredlogs.install(
level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")



''' start dmu '''
dmuObj = Dmu()

''' Start http server '''
httpSrvObj = httpSrv(dmuObj, {})
httpSrvObj.attachSubscriber("0.0.0.0", 8087, "")

''' Start mqtt client '''
mqttObj = MqttClient("localhost", dmuObj)




#create elements
dmuObj.add([], "listTest")
dmuObj.add({"level1" : { "level2" : "testval", "level21" : "testval" }}, "dictTest")
dmuObj.add({"level1" : { "list" : [] }}, "mixedTest")

#register handlers on different elements and subelements
#dmuObj.addRx(listChanged,"listTest")
mqttObj.autoAttachPublisher("dictTest", "level1", "level2")
mqttObj.autoAttachPublisher("dictTest", "level1", "level21")

mqttObj.attachSubscriber("/dictTest/level1/level2", "json", "dictTest","level1","level2")
mqttObj.attachSubscriber("/dictTest/level1/level21", "json", "dictTest","level1","level21")

#dmuObj.setDataSubset({"level2" : [1,2,3], "level21" : "hmm"}, "dictTest", "level1")#overwrite a list

level2Uuid = dmuObj.addElmEvent(listChanged, "dictTest", "level1", "level2")
level21Uuid = dmuObj.addElmEvent(listChanged, "dictTest", "level1", "level21")


while True:
    time.sleep(1)
    lastUpdate2 = dmuObj.eventLastUpdate("dictTest",level2Uuid)
    lastUpdate21 = dmuObj.eventLastUpdate("dictTest",level21Uuid)
    logging.info("Element %s->%s was last updated %d", "dictTest", "->".join(["level1","level2"]), lastUpdate2)
    logging.info("Element %s->%s was last updated %d", "dictTest", "->".join(["level1","level21"]), lastUpdate21)

logging.info("end")
sys.exit(0)

