# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

import sys
from pathlib import Path

# Add project path to system path to facilitate imports of teebco submodules
project_dir = Path(__file__).resolve().parents[1]
sys.path.append(str(project_dir))

from dmu.dmu import Dmu
from dmu.mqttClient import MqttClient
import coloredlogs, logging
import time, sys

def listChanged(data, uuid, name, args, context):
    logging.info("list changed")


coloredlogs.install(
level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")

dmuObj = Dmu()

mqttObj = MqttClient("localhost", dmuObj)




#create elements
dmuObj.add([], "listTest")
dmuObj.add({"level1" : { "level2" : "testval" }}, "dictTest")
dmuObj.add({"level1" : { "list" : [] }}, "mixedTest")

#register handlers on different elements and subelements
#dmuObj.addRx(listChanged,"listTest")

mqttObj.attachPublisher("/test-topic", "json", "dictTest", "level1", "level2")

dmuObj.setData([1,2,3],"dictTest","level1","level2") #overwrite a list



while True:
    time.sleep(1)

logging.info("end")
sys.exit(0)

