# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

import sys
from pathlib import Path

# Add project path to system path to facilitate imports of teebco submodules
project_dir = Path(__file__).resolve().parents[2]
sys.path.append(str(project_dir))

import coloredlogs, logging, time
from dmu.dmu import dmu
from dmu.fiwareClient import fiwareClient

def testReceiver(data, uuid, name, args, context):
    logging.info(data)



def main():
    coloredlogs.install(
    level='DEBUG',
    fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
    field_styles=dict(
        asctime=dict(color='green'),
        hostname=dict(color='magenta'),
        levelname=dict(color='white', bold=True),
        programname=dict(color='cyan'),
        name=dict(color='blue')))

    logging.info("Program Start")

    ''' start dmu '''
    dmuObj = dmu()

    exampleConf = {
        "fiwareHost" : "localhost",                     # Hostname of fiware instance
        "fiwarePort" : 1026,                            # Port of fiware instance
        "subscriptionHost" : "host.docker.internal",    # Ip for receiving messages from fiware
        "subscriptionListener" : "0.0.0.0",             # Address to bin the listener to
        "subscriptionPort" : 8081,                      # Port for receiving messages from fiware
        "protocol" : "http"                             # Communication protocol  
    }

    dmuObj.add({}, "testReceive")
    dmuObj.addElmEvent(testReceiver, "testReceive")

    fiwareObj = fiwareClient(dmuObj, exampleConf)

    enityId = "myEntity"
    fiwareObj.createEntity(enityId)

    fiwareObj.attachSubscriber(enityId, "testReceive")

    testData = 'Example test2'

    while True:
        time.sleep(1)
    receivedData = dmuObj.getData("testReceive")
    dmuObj.rm("testReceive")    

    logging.info("Program End")
    sys.exit(0)


if __name__=="__main__":
    main()