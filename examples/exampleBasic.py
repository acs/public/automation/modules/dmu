# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.
import sys
from pathlib import Path

# Add project path to system path to facilitate imports of the digital twin submodules
project_dir = Path(__file__).resolve().parents[1]
sys.path.append(str(project_dir))

import time

from dmu.dmu import dmu
import coloredlogs, logging


coloredlogs.install(level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")


def listChanged(data, uuid, name, args, context):
    logging.info("list changed")

def dictChanged(data, uuid, name, args, context):
    logging.info("dict changed")

def subelementChanged(data, uuid, name, args, context):
    logging.info("subelement changed")

def mixedListChanged(data, uuid, name, args, context):
    logging.info("mixed list changed")

def mixedSubelementhanged(data, uuid, name, args, context):
    logging.info("mixed subelement changed")


def pointerTest(data, uuid, name, args, context):
    context.test()
    logging.info("pointerTest")


def main():
    dmuObj = dmu()


    #create elements
    dmuObj.add([], "listTest")
    dmuObj.add({"level1" : { "level2" : "testval" }}, "dictTest")
    dmuObj.add({"level1" : { "list" : [] }}, "mixedTest")

    #register handlers on different elements and subelements
    listTestUUID = dmuObj.addElmEvent(listChanged,"listTest")
    dictTestUUID = dmuObj.addElmEvent(listChanged,"dictTest")
    subDictTestUUID = dmuObj.addElmEvent(subelementChanged,"dictTest","level1","level2")
    subMixedTestUUID = dmuObj.addElmEvent(mixedListChanged,"mixedTest","level1","list")
    mixedTestUUID = dmuObj.addElmEvent(mixedSubelementhanged,"mixedTest","level1")


    testObj = testClass()
    contextTestUUID = dmuObj.addElmEvent(pointerTest,"mixedTest","level1", context=testObj)



    dmuObj.setData({"level1" : { "level2" : "newValue1" }},"dictTest")#overwrite the whole dict
    dmuObj.setData("newValue2","dictTest","level1","level2")#overwrite just one element

    dmuObj.setData([1,2,3],"listTest")#overwrite a list

    dmuObj.setData({ "list" : [1,2,3] },"mixedTest","level1")#overwrite multiple subelments

    dmuObj.setData([4,5,6],"mixedTest","level1","list")#overwrite a list

    #cleanup
    
    time.sleep(1)
    
    # explicit cleanup
    dmuObj.cleanup()

    logging.info("end")
    

class testClass:
    def test(self):
        logging.info("pointerTest")

if __name__ == "__main__":
    main()


