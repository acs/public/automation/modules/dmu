# This is an example for interacting with fiware


# To reset the contextbroaker you can remove all data of all containers

ATTENTION: This removes all docker content not only the one from fiware. To be fixed!!

`docker system prune`
`docker volume prune`


# Start the container:

`docker-compose up`

# Read current status

`http://10.100.2.129:1026/v2/entities/`

# exmapleFiwareSubSocket

This example provides a translator from fiware to socket. This can be used together with a matlab receiver or villasNode
