# Copyright (c) 2020 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

import coloredlogs, logging, threading, time
from dmu.httpSrv import httpSrv
from dmu.dmu import dmu

dmuObj = object#define dmu object variable
clientCount = 0#number of registerd clients

def registerClient(data, uuid, name, args, context):
    http_provider: httpSrv = context["httpProvider"]
    
    socket = data["hostname"] + ":" + str(data["port"])
    logging.info("Client event: %s", data)
    if not dmuObj.elmExists("clients", socket):
        http_provider.attachPublisher(data["hostname"], data["port"], "clients")
        dmuObj.addSubElm({"init" : "false"}, "clients", socket)
        
        dmuObj.setData({"init" : "true"}, "clients", socket) #trigger a sync event
        logging.info("Registered client with socket: %s", socket)
        dmuObj.setData((dmuObj.getData("control", "clientCount") + 1), "control", "clientCount")


coloredlogs.install(
level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")

''' start dmu '''
dmuObj = dmu()

''' Start http server '''
httpProvider = httpSrv(dmuObj, {})

controllerDef = {
    "clientCount" : 0
}

dmuObj.add(controllerDef, "control")

dmuObj.add({}, "regObj")#this object is used to register new clients
dmuObj.addElmEvent(registerClient, "regObj",context={"httpProvider": httpProvider})

dmuObj.add({}, "clients")

httpProvider.attachSubscriber("0.0.0.0", 8080, "regObj")

#Make sure to set the correct number of clients here.
#It should be the same as the docker compose scale parameter
while dmuObj.getData("control", "clientCount") < 10:
    time.sleep(1)
    logging.info("Client count %s", str(dmuObj.getData("control", "clientCount")))

'''
This was the basic registration of clients. 
After this ther is a data structure which is synced to the client as long as the
whole structure is written.
'''

logging.info("All clients are registered")

while True:
    time.sleep(1)