# Copyright (c) 2021 Manuel Pitz, RWTH Aachen University
#
# Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
# http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
# http://opensource.org/licenses/MIT>, at your option. This file may not be
# copied, modified, or distributed except according to those terms.

import random
import socket, time
from dmu.httpSrv import httpSrv
import coloredlogs, logging, threading
from dmu.dmu import dmu

dmuObj = object#define dmu object variable

def controllerUpdate(data, uuid, name, args):
    logging.info("Controller Update: %s", str(data))


coloredlogs.install(
level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")


''' start dmu '''
dmuObj = dmu()

''' Start http server '''

myHostname = socket.gethostname()
dmuObj.add({}, "clients")
dmuObj.add({}, "regObj")

httpProvider = httpSrv(dmuObj, {})
port = random.randrange(8081, 8999)
httpProvider.attachSubscriber("0.0.0.0", port, "clients")
httpProvider.attachPublisher("0.0.0.0", 8080, "regObj")#this is not correct. we cannot send to 0.0.0.0

mySocket = myHostname + ":" + str(port)

dmuObj.addSubElm({}, "clients", mySocket)
dmuObj.addElmEvent(controllerUpdate, "clients", mySocket)

# register client
dmuObj.setData({"hostname": myHostname, "port": port},"regObj")


while not (dmuObj.elmExists("clients", mySocket, "init") and dmuObj.getData("clients", mySocket, "init") == "true"):
    time.sleep(1)

logging.info("Registration of %s successful!", mySocket)


'''
This was the basic registration of clients. 
Now you can use the client structure to gather commands from the controller
'''

while True:
    time.sleep(1)