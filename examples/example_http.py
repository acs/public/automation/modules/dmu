from dmu.dmu import dmu
from dmu.httpSrv import httpSrv
import coloredlogs, logging
import time
coloredlogs.install(level='DEBUG',
fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
field_styles=dict(
    asctime=dict(color='green'),
    hostname=dict(color='magenta'),
    levelname=dict(color='white', bold=True),
    programname=dict(color='cyan'),
    name=dict(color='blue')))
logging.info("Program Start")

def elementChanged(data, uuid, name, args, context):
    logging.info("%s element changed %s data: %s", context, name, data)



dmuObjServer = dmu()
dmuObjClient = dmu()

dmuObjServer.add({"level1": "testval"}, "test")
dmuObjServer.addElmEvent(elementChanged, "test", context="server")

dmuObjClient.add({"level1": ""}, "test")
dmuObjClient.addElmEvent(elementChanged, "test", context="client")

serverProvider = httpSrv(dmuObjServer, {})
clientProvider = httpSrv(dmuObjClient, {})

clientProvider.attachSubscriber("localhost", 8000, "test")

time.sleep(0.1)

uuid = serverProvider.attachPublisher("localhost", 8000, "test")

dmuObjServer.setData({"level1": "newtestval"}, "test")


time.sleep(0.1)

dmuObjServer.setData({"level1": "newtestval2", "level1.2": {"level2": "wow"}}, "test")

time.sleep(0.1)

dmuObjServer.setData("it work's", "test", "level1.2", "level2")
dmuObjServer.notifyElmEvent("test", str(uuid))